/*
1. mostra menu de opções numeradas
2. aguarda digitação do opção escolhida
2.1 testa se a opção é valida. esta no conjunto de opcoes possivel)
2.2 se for válida, execute a opçãpo associando o número da opção.
3. executa função associada ao número da opção
3.1 senão: informa que a opção é inválida
4. volta para 
 Exemplo:
 1 - Cadastrar
 2 - Consultar
 3 - Sair
 >

 Digitou 1, então executa função cadastrar()
 Digitou 2, então executa fun~ção consultar()
 Digitou 3, sai do loop do programa.
*/
import { question } from "readline-sync"
/* PODERIA SER ASSIM \/, MAS QUANDO COLOCA * ELE PUXA TUDO DO MÓDULO:
import { cadastrar, listar, buscarDoadorPorDataUltimaDoacao, buscarDoadorPorTipoSanguineo } from "./hemo-funcoes.mjs";
*/
import * as funcoes from './funcoes.mjs'; /* AS HEMO cria um nome especifico para as acoes do modulo*/
let opcao; 

menu()

export function menu() {
while(true) {
    console.clear()                          /* >>>>>>  aqui limpa a tela pra rodar nova opcao */
    console.log(
`
==== SISTEMA DE CADASTRO E CONSULTA DE PARTICULARIDADES DE PAISES - PARA EXPORTAÇÃO ====
1 - Listar paises com particularidades cadastradas
2 - Buscar particularidades por pais
3 - Buscar pais por regiao / continente
4 - Cadastrar novo pais
5 - Sobre
6 - Sair
`
        );
    opcao = parseInt(question('Escolha uma opcao:'));

switch (opcao) {
    case 1:
        funcoes.listar();
        break;
    case 2:
        funcoes.buscarParticularidadePorPais();
        break;
    case 3:
        funcoes.buscarParticularidadePorRegiao();
        break;
    case 4:
        funcoes.cadastrar();
        break;
    case 5:
        funcoes.sobre();
        break;
    case 6:
        console.log('Fim do programa')
        process.exit(0)
    default:
        console.log('Opção inválida');
        break;
}    
question('Pressione ENTER para continuar')      
}
}
/* REFACTORING > REFATURAÇÃO DO CODIGO = QUANDO TEU CODIGO MELHORA
FIZEMOS ISSO NA AULA */