
import { question } from "readline-sync";

import * as funcao from './menu.mjs'
 //ssss//

/*/let regioes = [
        {regCode: 0, regiaoPais: ''},
        {regCode: 1, regiaoPais: 'Africa'},
        {regCode: 2, regiaoPais: 'America Central'},
        {regCode: 3, regiaoPais: 'America do Norte'},
        {regCode: 4, regiaoPais: 'America do Sul'},
        {regCode: 5, regiaoPais: 'Asia'} ,
        {regCode: 6, regiaoPais: 'Europa'},
        {regCode: 7, regiaoPais: 'Oriente Medio'}
    ]
/*/

let dados = [
    {pais: 'Angola', regiaoPais:'Africa', regCode: 1, obsPais: 'Se maritmo OBRIGATORIO CNCA',exeProcesso : '1245/23'},
    {pais: 'Uruguai', regiaoPais: 'America do Sul', regCode: 4, obsPais: 'ACORDO ACE 18 - COD',exeProcesso : '7893/23'},
    {pais: 'Estados Unidos', regiaoPais: 'America do Norte', regCode: 3, obsPais: 'Se maritimo, OBRIGATORIO ISF', exeProcesso : '458/23'},
    {pais: 'Bolivia', regiaoPais: 'America do Sul', regCode: 4, obsPais: `ACE 36 - CO fisico`,exeProcesso : '1323/23'},
    {pais: 'Chile', regiaoPais: 'America do Sul', regCode: 4, obsPais: `ACE 35 - CO fisico, emitir ACIU`,exeProcesso : '365/23'},
    {pais: 'China', regiaoPais: 'Asia', regCode: 5, obsPais: `No BL Consignee/Notify constar o USCI nbr`, exeProcesso : '3666/23'},
    {pais: 'Costa do Marfim', regiaoPais: 'Africa', regCode: 1, obsPais: `Se maritimo, Necessário BSC NUMBER`, exeProcesso : '3154/23'},
    {pais: 'Cuba', regiaoPais: 'America Central', regCode: 2, obsPais: `Geralmente LC e inspeção da carga`, exeProcesso : '3154/23'},
    {pais: 'Egito', regiaoPais: 'Africa', regCode: 1, obsPais: `Ha acordo EGITO, Pre registro Cargo-X`, exeProcesso : '20712/21'},
    {pais: 'Emirados Arabes', regiaoPais: 'Oriente Medio',regCode: 7, obsPais: `Ver necessidade de CO na Camara de Comercio Arabe`, exeProcesso : '4064/22'},
    {pais: 'Libia', regiaoPais: 'Africa', regCode: 1, obsPais: `ECTN e docs c carimbo da Camara de Comercio.`, exeProcesso : '29674/22'},
    {pais: 'Peru', regiaoPais: 'America do Sul', regCode: 4, obsPais: `ACE 58 - CO fisico se Maritmo BL FRETADO`, exeProcesso : '7526/23'},
    {pais: 'Espanha', regiaoPais: 'Europa', regCode: 6, obsPais: `CO COMUM, quando necessario`, exeProcesso : '7266/23'},

]

export function listar() {
    console.log('Rodando listagem...')

    console.table(dados)
}
export function buscarParticularidadePorPais() {
    let paisPesquisa = question("Digite o pais que deseja buscar: ");
    let encontrouPais = false;

    for (let index = 0; index < dados.length; index++) {
        const paisBusca = dados[index];
        if (paisBusca.pais == paisPesquisa) {
            console.table(paisBusca)
            encontrouPais = true;
        }
    }
    if(!encontrouPais) {
        console.log(`O país ${paisPesquisa} não consta em nossa base de dados.`);
        let boraCadastrar = question(`Deseja cadastrar ${paisPesquisa} ?
        1 - Sim
        2 - Nao

        `);
     if (boraCadastrar == 1) {return cadastrar() }
     if (boraCadastrar == 2) {console.log('Voltando ao menu princiapl...')}{ return funcao.menu() }
       
    }
        
}

export function buscarParticularidadePorRegiao() {

while(true) {
    console.clear()
    console.log(`Digite a opcao desejada para realizar a consulta por regiao / continente: 

1 - Africa 
2 - America Central
3 - America do Norte
4 - America do Sul
5 - Asia
6 - Europa
7 - Oriente Medio
8 - Voltar ao menu anterior`)

const pesquisaRegiao = parseInt(question('Escolha uma opcao:'));
switch (pesquisaRegiao) {
    case 1:
    console.log('Pesquisando países na Africa')
    for (let index = 0; index < dados.length; index++) {
    const regiaoBuscada = dados[index];
    if (regiaoBuscada.regCode == pesquisaRegiao) {
     console.table(regiaoBuscada)} 
 }	
    break;
    case 2:
    console.log('Pesquisando países na America Central')
    for (let index = 0; index < dados.length; index++) {
    const regiaoBuscada = dados[index];
    if (regiaoBuscada.regCode == pesquisaRegiao) {
     console.table(regiaoBuscada)} 
 }	
     break;
    case 3:
    console.log('Pesquisando países na America do Norte')
    for (let index = 0; index < dados.length; index++) {
    const regiaoBuscada = dados[index];
    if (regiaoBuscada.regCode == pesquisaRegiao) {
    console.table(regiaoBuscada)} 
 }	
        break;
    case 4:
    console.log('Pesquisando países na America do Sul')
    for (let index = 0; index < dados.length; index++) {
    const regiaoBuscada = dados[index];
    if (regiaoBuscada.regCode == pesquisaRegiao) {
     console.table(regiaoBuscada)} 
 }	
    break;
    case 5:
    console.log('Pesquisando países na Asia')
    for (let index = 0; index < dados.length; index++) {
    const regiaoBuscada = dados[index];
    if (regiaoBuscada.regCode == pesquisaRegiao) {
     console.table(regiaoBuscada)} 
 }	
    break;
    case 6:
    console.log('Pesquisando países na Europa')
    for (let index = 0; index < dados.length; index++) {
    const regiaoBuscada = dados[index];
    if (regiaoBuscada.regCode == pesquisaRegiao) {
    console.table(regiaoBuscada)}
} 
  	break;
    case 7:
    console.log('Pesquisando países no Oriente Medio')
    for (let index = 0; index < dados.length; index++) {
    const regiaoBuscada = dados[index];
    if (regiaoBuscada.regCode == pesquisaRegiao) {
     console.table(regiaoBuscada)} 
 }	
    break;
    case 8:
    console.log('Voltando ao meu anterior...')
    return false
    break
    default:
    console.log(`Opção inválida.`);
    break;
}    
question('Pressione ENTER para continuar')      
}

}

export function cadastrar() { 
    console.log('Cadastrando novo pais...');
    let paisNovo = question("Digite o pais que deseja cadastar: ");
    let paisJacadstrado = false;

    for (let index = 0; index < dados.length; index++) {
        const paisBuscaEmCadastrar = dados[index];
        if (paisBuscaEmCadastrar.pais == paisNovo) {
            console.log(`${paisNovo} ja cadastrado...`)
            console.table(paisBuscaEmCadastrar)
            paisJacadstrado = true;
        }
    }

    if(!paisJacadstrado) {
        let regCodeNovo = parseInt(question(`Digite o numero que represente o continente / regiao de ${paisNovo}:
        1 - Africa 
        2 - America Central
        3 - America do Norte
        4 - America do Sul
        5 - Asia
        6 - Europa
        7 - Oriente Medio
        
        `));

        let particularidadeNova = question(`Digite a particularidade de ${paisNovo} que deseja cadastrar: `);
        let exemploProcessoPaisNovo = question(`Digite um exemplo de processo de processos de exportacao ja realizado para ${paisNovo}: `);
        
            
        let regiaoNome = new Map();
        regiaoNome.set(1,'Africa');
        regiaoNome.set(2,'America Central');
        regiaoNome.set(3,'America do Norte');
        regiaoNome.set(4,'America do Sul');
        regiaoNome.set(5,'Asia');
        regiaoNome.set(6,'Europa');
        regiaoNome.set(7,'Oriente Medio');

        /*    
        let regiaoNome = function regiaoPorCodigo (regiaoNome) {
        /*let regiaoNome = ''
        if (regCodeNovo == 1) {return 'Africa'}
        if (regCodeNovo == 2) {return 'America Central'}
        if (regCodeNovo == 3) {return 'America do Norte'}
        if (regCodeNovo == 4) {return 'America do Sul'}
        if (regCodeNovo == 5) {return 'Asia'}
        if (regCodeNovo == 6) {return 'Europa'}
        if (regCodeNovo == 7) {return 'Oriente Medio'}
        } 
        */

        dados.push(
            {
                pais: paisNovo,
                regCode: regCodeNovo,
                regiaoPais: regiaoNome.get(regCodeNovo),
                obsPais: particularidadeNova,
                exeProcesso : exemploProcessoPaisNovo,
            }
        );                
    }
}


export function sobre() {
    console.log('Mostrando dados sobre o programa...')
    
    while(true) {
        console.clear()                          /* >>>>>>  aqui limpa a tela pra rodar nova opcao */
        console.log(
    `
    ==== SOBRE O PROGRAMA DE CADASTRO E CONSULTA DE PAISES ====
    1 - Objetivo do sistema
    2 - Fontes de consulta
    3 - Wiki
    4 - Volar ao menu anterior
    
    `);
    
    const opcaoSobre = parseInt(question('Escolha uma opcao:'));
    switch (opcaoSobre) {
        case 1:
            console.log(`
            O OBJETIVO do programa é facilitar a consulta de particularidades dos paises para onde se deseja exportar.
            A ideia do programa surgiu devido a dificuldade dos accounts de exportacao, ao receber um novo processo,
            conseguir identificar as particulatidade para determinado pais, e assim, conseguir realizar o processo
            de exportacao com o maximo de informacoes necessarias.

            Desta forma, acreditamos que o programa ira proporcionar agilidade no processo de exportacao, reducao de custos
            caso for identificado alguma necessidade ANTES de seu embarque ao exterior e tambem autonomia para os 
            accounts em realizar seu processo com maior seguranca.

            Acreditamos no compartilhamento de informacaoes e por isto, desejamos criar um banco de dados util para
            os accounts e clientes.
            `);
            break;
        case 2:
            console.log(`
            Com anos de experiencia do usuario, o programa esta alimentado com informacoes obtidas pela experiencia do
            programador alem de contar com a base de dados de uma empresa com mais 30 anos de experiencia com registros 
            no atual ERP.
            Os dados considerados para cadastro no banco de dados sao de no maximo 24 meses.
            `);
            break;
        case 3:
            console.log(`
            Em caso de duvidas, encontre a seguir o wiki sobre os campos da base de dados:
            
            Pais: Deve-se considerar o pais para onde sera realizada a exportacao. O nome deve ser em portugues com a primeira
            letra em maiusculo.
            
            regiaoPais: Os paises neste banco de dados estao localizados em continemtes / regioes conforme abaixo:
            Africa, America Central, America do Norte, America do Sul, Asia, Europa e Oriente Medio.
            O banco de dados considerado para esta relacao e o Siscomex.
                        
            obsPais: Neste campo, sera considerada toda particulatidade, observacao ou requisito que o usuario achar necessario
            registrar.
            Como instrucao para preenchimento, sempre considerar processos conforme ERP da empresa do estudo, onde haja a
            comprovacao da informacao descrita. Como documento anexado, email ou registro nos requistos. 
            
            exeProcesso : Aqui, considera-se o EXEMPLO DE PROCESSO onde o usuario podera acessar diretamente o ERP da empresa
            em referencia e identificar atraves de comprovacoes as informacoes descritas.
            `);
            break;
        case 4:
            console.log('Voltando ao meu anterior...')
            //process.exit(0)
           return false
            // window.location.href = menu.mjs;
            break;
        default:
            console.log('Opção inválida');
            break;
    }    
    question('Pressione ENTER para continuar')      
    }

}
